import React from "react";
import axios from "axios";
import Chart from "react-apexcharts";
import "./sty.css";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";
import * as getDateActions from "../../../actions/getDate.action";


const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  header: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
}));

export default function Europe(porps) {
 
  const [dataCovid, setdataCovid] = React.useState([]);
  const [nameCountry, setnameCountry] = React.useState([]);
  const [date, setDate] = React.useState("12/11/20");
  const classes = useStyles();
  const dispatch = useDispatch();
  const getDateReducer = useSelector(({ getDateReducer }) => getDateReducer);


  const handleChange = (event) => {
    setDate(event.target.value);
  };

  React.useEffect(() => {

    async function fetData() {
      var dataCovidArray = [];
      var nameCountryArray = [];
      const request = await axios.get(
        "https://api-covid-v1.herokuapp.com/api/datacovid",
        {
          params: {
            date:date,
            continent:"Europe"
          },

        }
      );

      request.data.result_data.forEach((item) => {
        nameCountryArray.push(item.country);
        dataCovidArray.push(item.cases);
      });

      setdataCovid(dataCovidArray);
      setnameCountry(nameCountryArray);
    }

    fetData();

 

  }, [date]);


  React.useEffect(() => {
          dispatch(getDateActions.getDate());
    
  }, [dispatch])



  const chart = {
    series: [
      {
        data: dataCovid
      }
    ],
    options: {
      chart: {
        type: "bar",
        width: "100%",
      },

      plotOptions: {
        bar: {
          barHeight: "70%",
          distributed: true,
          rangeBarOverlap: true,
          rangeBarGroupRows: true,
          horizontal: true,
          dataLabels: {
            position: "bottom",
          },
        },
      },
      colors: [
        "#33b2df",
        "#546E7A",
        "#d4526e",
        "#13d8aa",
        "#A5978B",
        "#2b908f",
        "#f9a3a4",
        "#90ee7e",
        "#f48024",
        "#69d2e7",
      ],
      dataLabels: {
        enabled: true,
        textAnchor: "start",
        style: {
          colors: ["#fff"],
        },
        formatter: function (val, opt) {
          return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val + " คน";
        },
        offsetX: 0,
        dropShadow: {
          enabled: true,
        },
      },
      stroke: {
        width: 1,
        colors: ["#fff"],
      },
      xaxis: {
        categories: nameCountry,
      },
      yaxis: {
        labels: {
          show: false,
        },
      },

      tooltip: {
        theme: "dark",
        x: {
          show: false,
        },
        y: {
          title: {
            formatter: function () {
              return "";
            },
          },
        },
      },
    },
  };

  return (
    <div>
      <div className={classes.header}>
        <h3> รายงานผู้ติดเชื่อ ทวีปยุโรป </h3>
        <span >กรุณาเลือกวันที่</span>
        <div>
          <FormControl variant="outlined" className={classes.formControl} style={{marginTop:'2em'}}>
            <InputLabel id="demo-simple-select-outlined-label">Date</InputLabel>
            <Select
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
              value={date}
              defaultValue="12/10/20"
              onChange={handleChange}
              label="Date"
            >
              {
                getDateReducer.result ?   getDateReducer.result.map(item =>{
                        
                  if(item != undefined){
                        return (
                             <MenuItem value={item}>{item}</MenuItem>
                        )

                  }
               
                }): null
              }

          
            </Select>
          </FormControl>
        </div>
      </div>

      <div class="graphic-container">
        <Chart options={chart.options} series={chart.series} type="bar" />
      </div>
    
      
    </div>
  );
}
