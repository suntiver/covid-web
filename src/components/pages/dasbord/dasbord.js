import Chart from 'react-apexcharts'
import React from 'react'
import * as getDataAction from  "../../../actions/getData.action";
import { useDispatch, useSelector } from "react-redux";
import './sty.css'



export default function Dasbord() {
   
    const [dataCovid, setdataCovid] = React.useState([]);
    const [nameCountry, setnameCountry] = React.useState([]);

    const dispatch = useDispatch();
    const getDataReducer = useSelector(({ getDataReducer }) => getDataReducer);

    var dataArray = []
    React.useEffect(() => {
             dispatch(getDataAction.getCovidAll("11/11/20"));
           
            // console.log(getDataReducer.result);
     
    }, [dispatch]);



    const chart = {
        series: [{
            data: [20,30,40,30,40,30,40,30,40,30,40]
          }],
          options: {
            chart: {
              type: 'bar',
              height: 300
            },
            plotOptions: {
              bar: {
                barHeight: '100%',
                distributed: true,
                horizontal: true,
                dataLabels: {
                  position: 'bottom'
                },
              }
            },
            colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e',
              '#f48024', '#69d2e7'
            ],
            dataLabels: {
              enabled: true,
              textAnchor: 'start',
              style: {
                colors: ['#fff']
              },
              formatter: function (val, opt) {
                return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val
              },
              offsetX: 0,
              dropShadow: {
                enabled: true
              }
            },
            stroke: {
              width: 1,
              colors: ['#fff']
            },
            xaxis: {
              categories: ['South Korea', 'Canada', 'United Kingdom', 'Netherlands', 'Italy', 'France', 'Japan',
                'United States', 'China', 'India'
              ],
            },
            yaxis: {
              labels: {
                show: false
              }
            },
           
         
            tooltip: {
              theme: 'dark',
              x: {
                show: false
              },
              y: {
                title: {
                  formatter: function () {
                    return ''
                  }
                }
              }
            }
          },
      }


    return (
        <div>
                    <div  >   
                        <h1>Covid</h1> 
                        <select name="cars" id="cars">
                            <option value="volvo">Volvo</option>
                            <option value="saab">Saab</option>
                            <option value="opel">Opel</option>
                            <option value="audi">Audi</option>
                        </select>
                        </div>
                   

                    <div class="graphic-container">
                        
                          <Chart options={chart.options} series={chart.series} type="bar"       />
                    </div>
                            
                     
        </div>
    )
}
