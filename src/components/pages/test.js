import React from 'react'
import axios from 'axios'
import Chart from 'react-apexcharts'
import './sty.css'


export default function Test() {
    const [dataCovid, setdataCovid] = React.useState([]);
    const [nameCountry, setnameCountry] = React.useState([]);
    React.useEffect(() => {
        

      //  async function  fetData(){
      //           var dataCovidArray = []
      //           var nameCountryArray = []
      //           const  request = await axios.get("http://localhost:5000/api/date",{
      //               params:{
      //                   date:"11/11/20"
      //                 }
      //           });
               
      //           request.data.result.forEach(item => {

      //               nameCountryArray.push(item[0])
      //               dataCovidArray.push(item[1])
      //           });
                
      //           setdataCovid(dataCovidArray)
      //           setnameCountry(nameCountry)

              
      //   }

      //   fetData();

    }, [])



    const chart = {
        series: [{
            data: [1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10]
          }],
          options: {
            chart: {
              type: 'bar',
              width: '100%',
              height:'100%'
              


            },
            
            plotOptions: {
              bar: {

                  columnWidth: '70%',
                  barHeight: '70%',
                  distributed: true,
                  rangeBarOverlap: true,
                  rangeBarGroupRows: true,
                  horizontal: true,
                  dataLabels: {
                    position: 'bottom'
                  },

              }
            },
            colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e',
              '#f48024', '#69d2e7'
            ],
            dataLabels: {
              
              enabled: true,
              textAnchor: 'start',
              style: {
                colors: ['#fff']
              },
              formatter: function (val, opt) {
                return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val
              },
              offsetX: 0,
              dropShadow: {
                enabled: true
              }
            },
            stroke: {
              width: 1,
              colors: ['#fff']
            },
            xaxis: {
              categories: ["1","2","3","4","5","6","7","8","9","10"]
            },
            yaxis: {
              
              
              labels: {
                show: false
              }
            },
           
         
            tooltip: {
              theme: 'dark',
              x: {
                show: false
              },
              y: {
                title: {
                  formatter: function () {
                    return ''
                  }
                }
              }
            }
          },
      }

    return (
        <div>
         
            <div class="graphic-container">
                        
                        <Chart options={chart.options} series={chart.series} type="bar"      />
            </div>
        </div>
    )
}
