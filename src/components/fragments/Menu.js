import React from "react";
import { NavLink } from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Home from "@material-ui/icons/Home";
import Assessment from "@material-ui/icons/Assessment";
import Notifications from "@material-ui/icons/Notifications";
import BarChart from "@material-ui/icons/BarChart";
const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: "auto",
  },
});

export default function Menu(props) {
  const classes = useStyles();

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom",
      })}
    >
      <List>
        <ListItem  component={NavLink}  to="/home" button key="home">
          <ListItemIcon>
          <Assessment />
          </ListItemIcon>
          <ListItemText primary="Home" />
        </ListItem>

        <ListItem component={NavLink} to="/asia" button key="asia">
          <ListItemIcon>
            <Assessment />
          </ListItemIcon>
          <ListItemText primary="Asia" />
        </ListItem>

        <ListItem  component={NavLink} to="/europe" button key="europe">
          <ListItemIcon>
          <Assessment />
          </ListItemIcon>
          <ListItemText primary="Europe" />
        </ListItem>

        <ListItem   component={NavLink} to="/africa" button key="africa">
          <ListItemIcon>
          <Assessment />
          </ListItemIcon>
          <ListItemText primary="Africa" />
        </ListItem>
        <ListItem  component={NavLink}  to="/notamerica" button key="notamerica">
          <ListItemIcon>
          <Assessment />
          </ListItemIcon>
          <ListItemText primary="North America" />
        </ListItem>
        <ListItem   component={NavLink} to="/southanerica" button key="southanerica">
          <ListItemIcon>
          <Assessment />
          </ListItemIcon>
          <ListItemText primary="South America" />
        </ListItem>
        <ListItem   component={NavLink} to="/oceania" button key="oceania">
          <ListItemIcon>
          <Assessment />
          </ListItemIcon>
          <ListItemText primary="Oceania" />
        </ListItem>
      </List>
    </div>
  );

  return (
    <div>
      <React.Fragment key={"left"}>
        <Drawer
          anchor={"left"}
          open={props.open}
          onClose={props.handleDrawerClose}
        >
          {list("left")}
        </Drawer>
      </React.Fragment>
    </div>
  );
}
