/* COVID */

export const  GETCOVID_FETCHING  = "GETCOVID_FETCHING";
export const  GETCOVID_FAILED    = "GETCOVID_FAILED";
export const  GETCOVID_SUCCESS  = "GETCOVID_SUCCESS";


/* Date */
export const  GETDATE_FETCHING  = "GETDATE_FETCHING";
export const  GETDATE_FAILED    = "GETDATE_FAILED";
export const  GETDATE_SUCCESS  = "GETDATE_SUCCESS";



/* sever */  
// export const  apiUrl = "https://disease.sh";
export const  apiUrl = "https://api-covid-v1.herokuapp.com";
export const  server = {

        // GET_COVID_ALL_URL : `/v3/covid-19/historical?lastdays=30`,
        GET_COVID_ALL_URL : `/api/datacovid-20`,
        GET_DATE_ALL_URL  : `/api/getdate`,
}

/*
    Error Code
*/
export const NOT_CONNECT_NETWORK = "NOT_CONNECT_NETWORK";
export const NETWORK_CONNECTION_MESSAGE ="Cannot connect to server, Please try again.";
