import {

    GETCOVID_FETCHING,
    GETCOVID_FAILED ,
    GETCOVID_SUCCESS,
    server
} from "../Constants";
import { httpClient } from "../utils/HttpClient";

const setStateToFetching = () => ({
    type:  GETCOVID_FETCHING,
  });
  

  export const setStateToFailed = (payload) => ({
    type: GETCOVID_FAILED,
    payload,
  });

  
  export const setStateToSuccess = (payload) => ({
    type: GETCOVID_SUCCESS,
    payload,
  });



  /* ดึงข้อมูล Covid  */
   export const  getCovidAll = (date) => {
    return (dispatch) => {
          dispatch(setStateToFetching());
          dogetCovidAll(date,dispatch);
    };
  }; 


  export const dogetCovidAll = async (date,dispatch) => {
   
    let result = await httpClient.get(server.GET_COVID_ALL_URL,{
      params:{
        date:date
      }
    });
    
    let {data }  = result;
   
    const  dataCovid = []
    data.result_data.map(item =>{
        dataCovid.push(item.cases)

    })

    
  
    if(data.length != 0){
          dispatch(setStateToSuccess(dataCovid));
    }else{
          dispatch(setStateToFailed());
    }
    

  };