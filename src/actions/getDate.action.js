import {

    GETDATE_FETCHING,
    GETDATE_FAILED ,
    GETDATE_SUCCESS,
    server
} from "../Constants";
import { httpClient } from "../utils/HttpClient";

const setStateToFetching = () => ({
    type:   GETDATE_FETCHING,
  });
  

  export const setStateToFailed = (payload) => ({
    type:  GETDATE_FAILED,
    payload,
  });

  
  export const setStateToSuccess = (payload) => ({
    type:  GETDATE_SUCCESS,
    payload,
  });



  /* ดึงข้อวันที่ออกมา  */
   export const  getDate = () => {
    return (dispatch) => {
          dispatch(setStateToFetching());
          dogetDate(dispatch);
    };
  }; 


  export const dogetDate = async (dispatch) => {
   
    let result = await httpClient.get(server.GET_DATE_ALL_URL);
    
    let {data }  = result;
        
  
    if(data.length != 0){
          dispatch(setStateToSuccess(data.result_date));
    }else{
          dispatch(setStateToFailed());
    }
    

  };