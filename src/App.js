import React from "react";
import Header from "./components/fragments/Header";
import Menu from "./components/fragments/Menu";
import { makeStyles } from "@material-ui/core/styles";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
import { Container } from "@material-ui/core";
import Home from "./components/pages/home/home";
import Africa  from "./components/pages/africa/africa";
import Asia   from "./components/pages/asia/asia";
import Europe from "./components/pages/europe/europe";
import NortAmerica from "./components/pages/nortAmerica/northAmerica";
import ocenia  from "./components/pages/oceania/oceania";
import Southamerica  from "./components/pages/southamerica/southAmerica";
import Oceania from "./components/pages/oceania/oceania";

const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: 0,
  },
}));
export default function App() {
  const classes = useStyles();
  const [openDrawer, setOpenDrawer] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpenDrawer(!openDrawer);
  };

  const handleDrawerClose = () => {
    setOpenDrawer(false);
  };

  return (
    <div>
      <Router>
        <Header handleDrawerOpen={handleDrawerOpen} open={openDrawer} />
        <Menu open={openDrawer} handleDrawerClose={handleDrawerClose} />
        <main className={classes.content} style={{ marginTop: "5%" }}>
          <Route path="/home" component={Home} />
          <Route path="/africa" component={Africa} />
          <Route path="/asia" component={Asia} />
          <Route path="/europe" component={Europe} />
          <Route path="/notamerica" component={NortAmerica} />
          <Route path="/oceania" component={Oceania} />
          <Route path="/southanerica" component={Southamerica} />
          <Route
            exact={true}
            path="/"
            component={() => <Redirect to="/home" />}
          />
        </main>
      </Router>
    </div>
  );
}
