import { combineReducers } from "redux";
import getDataReducer  from "./getData.reducer"
import getDateReducer  from "./getDate.reducer"

export default combineReducers({
    getDataReducer,
    getDateReducer
}); 
