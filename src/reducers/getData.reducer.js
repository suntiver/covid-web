import {

    GETCOVID_FETCHING,
    GETCOVID_FAILED ,
    GETCOVID_SUCCESS
} from "../Constants";

const initialState = {
    result: null,
    isFetching: false,
    isError: false,
};


export default (state = initialState, { type, payload }) => {
    switch (type) {
      case   GETCOVID_FETCHING:
        return { ...state, isFetching: true, isError: false, result: null };
      case    GETCOVID_FAILED :
        return { ...state, isFetching: false, isError: true,result: payload};
      case   GETCOVID_SUCCESS:
        return { ...state,isFetching: false, isError: false,result: payload};
      default:
        return state;
    }
  };