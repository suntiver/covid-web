import {

    GETDATE_FETCHING,
    GETDATE_FAILED ,
    GETDATE_SUCCESS
} from "../Constants";

const initialState = {
    result: null,
    isFetching: false,
    isError: false,
};


export default (state = initialState, { type, payload }) => {
    switch (type) {
      case   GETDATE_FETCHING:
        return { ...state, isFetching: true, isError: false, result: null };
      case    GETDATE_FAILED :
        return { ...state, isFetching: false, isError: true,result: payload};
      case   GETDATE_SUCCESS:
        return { ...state,isFetching: false, isError: false,result: payload};
      default:
        return state;
    }
  };